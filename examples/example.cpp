#include <fmt/format.h>
#include <rng.h>
#include <droptable.h>

namespace seeds
{
extern constexpr uint32_t reset = 0x01234567;
extern constexpr uint32_t links_house = 0x969743c1;
}

int main()
{
    auto *droptable = get_special_fx_droptable(
            5,
            false,
            &gAreaDroptables[0],
            0,
            24,
            0,
            0,
            0,
            0,
            0
            );
    return droptable->sum();
}
