#ifndef TMC_RNG_RNG_H
#define TMC_RNG_RNG_H
#include <bit>
#include <cstdint>

constexpr uint32_t Random(uint32_t &seed)
{
    seed = std::rotr(seed * 3, 13);
    return seed >> 1;
}

#endif//TMC_RNG_RNG_H
