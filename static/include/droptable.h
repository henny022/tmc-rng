#ifndef TMC_RNG_DROPTABLE_H
#define TMC_RNG_DROPTABLE_H

#include <cstddef>
#include <cstdint>
#include <stdexcept>

#include "rng.h"

enum struct ItemDrop : int
{
    NONE,
    RUPEE_GREEN,
    RUPEE_BLUE,
    RUPEE_RED,
    HEART,
    FAIRY,
    BOMBS,
    ARROWS,
    SHELL,
    KINSTONE_RED,
    KINSTONE_BLUE,
    KINSTONE_GREEN,
    BEETLE,
    NONE2,
    NONE3,
    NONE4,
};

enum struct Kinstone : int
{
    NONE,
    RW,
    RV,
    RE,
    BL,
    BS,
    GC,
    GG,
    GP,
};

constexpr ItemDrop kinstone_type(Kinstone kinstone)
{
    using enum Kinstone;
    switch (kinstone)
    {
        case NONE:
            return ItemDrop::NONE;
        case RW:
        case RV:
        case RE:
            return ItemDrop::KINSTONE_RED;
        case BL:
        case BS:
            return ItemDrop::KINSTONE_BLUE;
        case GC:
        case GG:
        case GP:
            return ItemDrop::KINSTONE_GREEN;
    }
    return ItemDrop::NONE;
}

#ifdef _MSC_VER
#define CONSTEXPR
#define IF_CONSTEVAL if (false)
#else
#define CONSTEXPR constexpr
#define IF_CONSTEVAL if consteval
#endif

struct Droptable {
    static constexpr auto size = 16;

    int16_t none;
    int16_t rupee1;
    int16_t rupee5;
    int16_t rupee20;
    int16_t heart;
    int16_t fairy;
    int16_t bombs;
    int16_t arrows;
    int16_t shell;
    int16_t kinstoneRed;
    int16_t kinstoneBlue;
    int16_t kinstoneGreen;
    int16_t beetle;
    int16_t none2;
    int16_t none3;
    int16_t none4;

    CONSTEXPR int16_t &operator[](ItemDrop item)
    {
        return this->operator[](static_cast<size_t>(item));
    }

    CONSTEXPR const int16_t &operator[](ItemDrop item) const
    {
        return this->operator[](static_cast<size_t>(item));
    }

    CONSTEXPR int16_t &operator[](size_t index)
    {
        IF_CONSTEVAL
        {
            switch (index)
            {
                case 0:
                    return none;
                case 1:
                    return rupee1;
                case 2:
                    return rupee5;
                case 3:
                    return rupee20;
                case 4:
                    return heart;
                case 5:
                    return fairy;
                case 6:
                    return bombs;
                case 7:
                    return arrows;
                case 8:
                    return shell;
                case 9:
                    return kinstoneRed;
                case 10:
                    return kinstoneBlue;
                case 11:
                    return kinstoneGreen;
                case 12:
                    return beetle;
                case 13:
                    return none2;
                case 14:
                    return none3;
                case 15:
                    return none4;
                default:
                    throw std::invalid_argument("bad droptable index");
            }
        }
        else
        {
            return reinterpret_cast<int16_t *>(this)[index];
        }
    }

    CONSTEXPR const int16_t &operator[](size_t index) const
    {
        IF_CONSTEVAL
        {
            switch (index)
            {
                case 0:
                    return none;
                case 1:
                    return rupee1;
                case 2:
                    return rupee5;
                case 3:
                    return rupee20;
                case 4:
                    return heart;
                case 5:
                    return fairy;
                case 6:
                    return bombs;
                case 7:
                    return arrows;
                case 8:
                    return shell;
                case 9:
                    return kinstoneRed;
                case 10:
                    return kinstoneBlue;
                case 11:
                    return kinstoneGreen;
                case 12:
                    return beetle;
                case 13:
                    return none2;
                case 14:
                    return none3;
                case 15:
                    return none4;
                default:
                    throw std::invalid_argument("bad droptable index");
            }
        }
        else
        {
            return reinterpret_cast<const int16_t *>(this)[index];
        }
    }

    constexpr void clamp()
    {
        for (auto i = 0u; i < size; ++i)
        {
            operator[](i) = std::max<int16_t>(operator[](i), 0);
        }
    }

    constexpr Droptable &operator+=(const Droptable &other)
    {
        for (auto i = 0u; i < size; ++i)
        {
            operator[](i) += other[i];
        }
        return *this;
    }

    [[nodiscard]] constexpr Droptable operator+(const Droptable &other) const
    {
        Droptable d{};
        for (auto i = 0u; i < size; ++i)
        {
            d[i] = operator[](i) + other[i];
        }
        return d;
    }

    [[nodiscard]] constexpr int16_t sum() const
    {
        int16_t s = 0;
        for (auto i = 0u; i < size; ++i)
        {
            s += operator[](i);
        }
        return s;
    }
};

static_assert(sizeof(Droptable) == sizeof(int16_t) * Droptable::size, "Droptable size mismatch");

struct DroptableModifiers {
    static constexpr auto NONE = 0;
    static constexpr auto NO_SHELLS = 1;
    static constexpr auto NO_KINSTONES = 2;
};

extern const Droptable gEnemyDroptables[];
extern const Droptable gAreaDroptables[];
extern const Droptable gAreaDroptables_EU[];
extern const Droptable gObjectDroptables[];
extern const Droptable gObjectDroptables_EU[];
extern const Droptable gUnk_0800191C[];
extern const Droptable gUnk_0800191C_EU[];
extern const Droptable gDroptableModifiers[];
extern const Kinstone gDroptableKinstones[3][0x40];

/* clang-format off */
const uint8_t gUnk_0811F960[] = {
        0x00, 0x00, 0x00, 0x11, 0x12, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x14, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x11, 0x11, 0x00, 0x00, 0x13, 0x00, 0x10, 0x10,
        0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
        0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00,
};

const uint8_t gUnk_0811F960_EU[] = {
        0x00, 0x00, 0x00, 0x11, 0x12, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x14, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x11, 0x11, 0x00, 0x00, 0x13, 0x00, 0x10, 0x10, 
        0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x00,
};
/* clang-format on */

constexpr void apply_droptable_modifiers(
        Droptable &droptable,
        uint8_t is_eu,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions)
{
    // picolyte
    if (gSave_stats_picolyteType != 0)
    {
        if (is_eu)
        {
            droptable += gEnemyDroptables[gSave_stats_picolyteType + 9];
        }
        else
        {
            droptable += gEnemyDroptables[gSave_stats_picolyteType + 6];
        }
    }

    if (gSave_stats_health <= 8)
    {
        droptable.heart += 5;
    }
    if (gSave_stats_bombCount == 0)
    {
        droptable.bombs += 3;
    }
    if (gSave_stats_arrowCount == 0)
    {
        droptable.arrows += 3;
    }
    if (gSave_stats_rupees <= 10)
    {
        droptable.rupee5 += 1;
    }

    if (gSave_stats_hasAllFigurines)
    {
        droptable += gDroptableModifiers[DroptableModifiers::NO_SHELLS];
    }
    if (gSave_didAllFusions)
    {
        droptable += gDroptableModifiers[DroptableModifiers::NO_KINSTONES];
    }
}

constexpr const Droptable *get_special_fx_droptable(
        uint8_t fx,
        uint8_t is_eu,
        const Droptable *gRoomVars_currentAreaDroptable,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions)
{
    auto table = is_eu ? gUnk_0811F960_EU[fx] : gUnk_0811F960[fx];
    if (table < 16)
    {
        return nullptr;
    }
    if (table > 24)
    {
        return nullptr;
    }
    if (!is_eu and table > 23)
    {
        return nullptr;
    }
    auto *droptable = new Droptable{};
    table -= 16;
    if (is_eu)
    {
        *droptable += gObjectDroptables_EU[table];
    }
    else
    {
        *droptable += gObjectDroptables[table];
    }
    *droptable += *gRoomVars_currentAreaDroptable;
    apply_droptable_modifiers(
            *droptable,
            is_eu,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions);
    droptable->clamp();
    return droptable;
}

/* clang-format off */
static const uint8_t DeathFx_EnemyDroptables[] = {
        0x6, 0x6, 0x6, 0x6, 0x0, 0xb, 0x0, 0x6, 0x6, 0x0, 0xa, 0x6, 0x6, 0x7, 0x6, 0x6,
        0x6, 0x0, 0x6, 0x6, 0x6, 0x8, 0x0, 0x8, 0x6, 0x6, 0x8, 0xa, 0x6, 0x0, 0x6, 0x0,
        0x6, 0x0, 0xa, 0x8, 0x6, 0x0, 0x6, 0x6, 0x6, 0x6, 0xb, 0x9, 0x6, 0x1, 0x6, 0x0,
        0x7, 0x6, 0x6, 0x6, 0x0, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6,
        0x7, 0x6, 0x7, 0x6, 0x6, 0x6, 0x8, 0x6, 0x6, 0x6, 0x6, 0x6, 0x8, 0x8, 0x6, 0x6,
        0x6, 0x6, 0xc, 0x0, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x0, 0x6, 0x6, 0x0, 0x6,
        0xc, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6,
};
/* clang-format on */

constexpr const Droptable *get_enemy_droptable(
        uint8_t enemy_id,
        uint8_t is_eu,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions)
{
    if (enemy_id >= 0xf0)
    {
        // TODO special drops
        return nullptr;
    }
    auto table = DeathFx_EnemyDroptables[enemy_id];
    if (table == 0)
    {
        return nullptr;
    }
    auto *droptable = new Droptable{};
    *droptable += gEnemyDroptables[table];
    apply_droptable_modifiers(
            *droptable,
            is_eu,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions);
    droptable->clamp();
    return droptable;
}

constexpr const Droptable *get_area_droptable(
        uint8_t is_eu,
        const Droptable *gRoomVars_currentAreaDroptable,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions)
{
    auto *droptable = new Droptable{};
    *droptable += *gRoomVars_currentAreaDroptable;
    apply_droptable_modifiers(
            *droptable,
            is_eu,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions);
    droptable->clamp();
    return droptable;
}

constexpr const Droptable *get_unk_droptable(
        int gRoomVars_unk2,
        bool is_eu,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions)
{
    // TODO can this be EU???? what even is dis???
    auto *droptable = new Droptable{};
    int index = gRoomVars_unk2 ? 1 : 0;
    if (is_eu)
    {
        *droptable += gUnk_0800191C_EU[index];
    }
    else
    {
        *droptable += gUnk_0800191C[index];
    }
    apply_droptable_modifiers(
            *droptable,
            is_eu,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions);
    droptable->clamp();
    return droptable;
}

constexpr void free_droptable(const Droptable *droptable)
{
    delete droptable;
}

constexpr ItemDrop simulate_item_drop(uint32_t &seed, const Droptable &droptable)
{
    auto sum = droptable.sum();
    auto rand = Random(seed);
    uint32_t item = rand >> 0x18;
    item &= 0xf;
    rand %= static_cast<uint32_t>(sum);
    for (size_t i = 0, j = 0; i < droptable.size; i++, item = (item + 1) & 0xf)
    {
        if ((j += static_cast<size_t>(droptable[item])) > rand)
        {
            break;
        }
    }
    return static_cast<ItemDrop>(item);
}

inline CONSTEXPR int find_random_drop(uint32_t seed, const Droptable &droptable, ItemDrop item)
{
    if (droptable[item] == 0)
    {
        return -1;
    }
    for (int i = 0;; ++i)
    {
        auto _item = simulate_item_drop(seed, droptable);
        if (_item == item)
        {
            switch (_item)
            {
                case ItemDrop::KINSTONE_RED:
                case ItemDrop::KINSTONE_GREEN: {
                    auto seed2 = seed;
                    auto rand = Random(seed2) & 0x3f;
                    if (rand != 0x3f)
                    {
                        return i;
                    }
                    break;
                }
                default:
                    return i;
            }
        }
    }
}

inline CONSTEXPR int find_kinstone_drop(uint32_t seed, const Droptable &droptable, Kinstone kinstone)
{
    if (droptable[kinstone_type(kinstone)] == 0)
    {
        return -1;
    }
    for (int i = 0;; ++i)
    {
        auto _item = simulate_item_drop(seed, droptable);
        switch (_item)
        {
            case ItemDrop::KINSTONE_RED:
            case ItemDrop::KINSTONE_BLUE:
            case ItemDrop::KINSTONE_GREEN: {
                auto type = static_cast<int>(_item) - static_cast<int>(ItemDrop::KINSTONE_RED);
                auto seed2 = seed;
                auto rand = Random(seed2) & 0x3f;
                auto _kinstone = gDroptableKinstones[type][rand];
                if (kinstone == _kinstone)
                {
                    return i;
                }
            }
            break;
            default:
                break;
        }
    }
}

#endif//TMC_RNG_DROPTABLE_H
