add_executable(test_rng test_rng.cpp)
target_link_libraries(test_rng project_settings static)
add_test(NAME test_rng COMMAND test_rng)
