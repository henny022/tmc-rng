#include <cstddef>
#include <rng.h>

using test_func = bool (*)();

bool test_period()
{
    const uint32_t initial_seed = 0x01234567;
    uint32_t seed = initial_seed;
    size_t i = 1;
    do
    {
        Random(seed);
        i++;
    } while (seed != initial_seed);
    return i == 822119801;
}

constexpr test_func tests[] = {test_period};
constexpr auto n_tests = (sizeof(tests) / sizeof(test_func));

int main()
{
    size_t successful = 0;
    for (const test_func test: tests)
    {
        if (test())
        { successful++; }
    }
    bool success = successful == n_tests;
    return !success;
}
