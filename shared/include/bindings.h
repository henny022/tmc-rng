#ifndef TMC_RNG_BINDINGS_H
#define TMC_RNG_BINDINGS_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef INTERFACE
#ifdef _MSC_VER
#define INTERFACE extern "C" __declspec(dllimport)
#else
#define INTERFACE extern "C"
#endif
#endif

INTERFACE const Droptable *tmcrng_get_special_fx_droptable(
        uint8_t fx,
        uint8_t is_eu,
        const Droptable *gRoomVars_currentAreaDroptable,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions);

INTERFACE const Droptable *tmcrng_get_enemy_droptable(
        uint8_t enemy_id,
        uint8_t is_eu,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions);

INTERFACE const Droptable *tmcrng_get_area_droptable(
        uint8_t is_eu,
        const Droptable *gRoomVars_currentAreaDroptable,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions);

INTERFACE const Droptable *tmcrng_get_unk_droptable(
        int gRoomVars_unk2,
        bool is_eu,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions);

INTERFACE ItemDrop tmcrng_simulate_item_drop(uint32_t seed, const Droptable *droptable);

INTERFACE int tmcrng_find_random_drop(uint32_t seed, const Droptable *droptable, ItemDrop item);

INTERFACE int tmcrng_find_kinstone_drop(uint32_t seed, const Droptable *droptable, Kinstone kinstone);

INTERFACE void tmcrng_free_droptable(const Droptable *droptable);

#ifdef __cplusplus
};
#endif

#endif//TMC_RNG_BINDINGS_H
