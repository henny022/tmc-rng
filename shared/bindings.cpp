#include "droptable.h"

#ifdef _MSC_VER
#undef INTERFACE
#define INTERFACE extern "C" __declspec(dllexport)
#endif

#include "bindings.h"

INTERFACE const Droptable *tmcrng_get_special_fx_droptable(
        uint8_t fx,
        uint8_t is_eu,
        const Droptable *gRoomVars_currentAreaDroptable,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions
)
{
    return get_special_fx_droptable(
            fx,
            is_eu,
            gRoomVars_currentAreaDroptable,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions
    );
}

INTERFACE const Droptable *tmcrng_get_enemy_droptable(
        uint8_t enemy_id,
        uint8_t is_eu,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions
)
{
    return get_enemy_droptable(
            enemy_id,
            is_eu,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions
    );
}

INTERFACE const Droptable *tmcrng_get_area_droptable(
        uint8_t is_eu,
        const Droptable *gRoomVars_currentAreaDroptable,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions
)
{
    return get_area_droptable(
            is_eu,
            gRoomVars_currentAreaDroptable,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions
    );
}

INTERFACE const Droptable *tmcrng_get_unk_droptable(
        int gRoomVars_unk2,
        bool is_eu,
        uint8_t gSave_stats_picolyteType,
        uint8_t gSave_stats_health,
        uint8_t gSave_stats_bombCount,
        uint8_t gSave_stats_arrowCount,
        uint16_t gSave_stats_rupees,
        uint8_t gSave_stats_hasAllFigurines,
        uint8_t gSave_didAllFusions
)
{
    return get_unk_droptable(
            gRoomVars_unk2,
            is_eu,
            gSave_stats_picolyteType,
            gSave_stats_health,
            gSave_stats_bombCount,
            gSave_stats_arrowCount,
            gSave_stats_rupees,
            gSave_stats_hasAllFigurines,
            gSave_didAllFusions
    );
}

INTERFACE ItemDrop tmcrng_simulate_item_drop(uint32_t seed, const Droptable *droptable)
{
    return simulate_item_drop(seed, *droptable);
}

INTERFACE int tmcrng_find_random_drop(uint32_t seed, const Droptable *droptable, ItemDrop item)
{
    return find_random_drop(seed, *droptable, item);
}

INTERFACE int tmcrng_find_kinstone_drop(uint32_t seed, const Droptable *droptable, Kinstone kinstone)
{
    return find_kinstone_drop(seed, *droptable, kinstone);
}

INTERFACE void tmcrng_free_droptable(const Droptable *droptable)
{
    free_droptable(droptable);
}
